package oauth

import (
	"net/http"
	"net/url"

	"gitlab.com/thegalabs/go/errors"
)

// GrantType is an enum for OAuth2 grant types
type GrantType string

// All allowed grant types
const (
	ClientCredentials GrantType = "client_credentials"
	Password          GrantType = "password"
	RefreshToken      GrantType = "refresh_token"
	AuthorizationCode GrantType = "authorization_code"
)

// GetGrantType retrieves the grant type from url values
func GetGrantType(params url.Values) GrantType {
	return GrantType(params.Get("grant_type"))
}

// ValidatePasswordGrant checks for a password grant type
func ValidatePasswordGrant(params url.Values) (username string, password string, err error) {
	username = params.Get("username")
	password = params.Get("password")

	if len(username) == 0 || len(password) == 0 {
		err = errors.NoResponse(http.StatusBadRequest)
	}
	return
}

// ValidateRefreshGrant checks for a refresh token grant type
func ValidateRefreshGrant(params url.Values) (refreshToken string, err error) {
	refreshToken = params.Get("refresh_token")
	if len(refreshToken) == 0 {
		err = errors.NoResponse(http.StatusBadRequest)
	}
	return
}

// ValidateAuthorizationCodeGrant checks or an authorization_code grant type
func ValidateAuthorizationCodeGrant(params url.Values) (code string, err error) {
	code = params.Get("code")
	if len(code) == 0 {
		err = errors.NoResponse(http.StatusBadRequest)
	}
	return
}
