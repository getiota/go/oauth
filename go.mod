module gitlab.com/thegalabs/go/oauth

go 1.16

require (
	cloud.google.com/go/storage v1.22.1
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.1
	gitlab.com/thegalabs/go/errors v0.3.0
	google.golang.org/api v0.80.0
	gorm.io/gorm v1.23.5
)
