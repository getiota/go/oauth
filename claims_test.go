package oauth_test

import (
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth"
)

func TestLimitScope(t *testing.T) {
	claims := oauth.Claims{Scope: []string{"hello", "zest", "march"}}

	claims.LimitScope([]string{"march", "noob"})

	assert.Equal(t, 1, len(claims.Scope))
	assert.Equal(t, "march", claims.Scope[0])
}

func TestEncodeClaims(t *testing.T) {
	claims := oauth.Claims{uuid.Must(uuid.NewV4()), "thegalabs.fr", "clt1-a", "clt1", "", []string{"user", "client"}, 0, 100}

	bytes, err := claims.Encode()

	assert.Nil(t, err)
	assert.NotNil(t, bytes)
}
