package oauth

import (
	"encoding/json"
	"fmt"

	"gitlab.com/thegalabs/go/oauth/utils"
)

func encodeClaims(claims interface{}) ([]byte, error) {
	raw, err := json.Marshal(claims)
	if err != nil {
		return nil, err
	}
	return utils.B64Encode(raw), nil
}

func jwtHeader(kid string) []byte {
	str := fmt.Sprintf(`{"alg":"ES256","typ":"JWT","kid":"%s"}`, kid)
	return utils.B64Encode([]byte(str))
}

// MakeJWT builds and signs a JWT
func MakeJWT(claims interface{}, signer Signer) (string, error) {
	encodedClaims, err := encodeClaims(claims)
	if err != nil {
		return "", err
	}

	id, err := signer.KeyID()
	if err != nil {
		return "", err
	}

	header := jwtHeader(id)

	signatureData := header
	signatureData = append(signatureData, byte('.'))
	signatureData = append(signatureData, encodedClaims...)

	signature, err := signer.Sign(id, signatureData)
	if err != nil {
		return "", err
	}

	return string(header) + "." + string(encodedClaims) + "." + string(utils.B64Encode(signature)), nil
}
