package oauth

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTokenResponseMarshalRefresh(t *testing.T) {
	generator := func(refresh string, found bool) func(t *testing.T) {
		return func(t *testing.T) {
			response := TokenResponse{"token", 100, "Bearer", refresh}

			b, err := json.Marshal(response)
			assert.Nil(t, err)

			var raw map[string]json.RawMessage
			err = json.Unmarshal(b, &raw)
			assert.Nil(t, err)

			_, f := raw["refresh_token"]
			assert.Equal(t, found, f)
		}
	}

	t.Run("Present", generator("Hello", true))
	t.Run("Absent", generator("", false))
}
