package oauth_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth"
)

func getFixturePath(name string) string {
	filePath, _ := os.Getwd()
	return path.Join(filePath, "./fixtures/"+name)
}

func TestUnmarshalBaseClient(t *testing.T) {
	bytes, err := ioutil.ReadFile(getFixturePath("client.json"))
	assert.Nil(t, err)

	var client oauth.BaseClient
	err = json.Unmarshal(bytes, &client)
	assert.Nil(t, err)
}

func TestUnmarshalClientList(t *testing.T) {
	bytes, err := ioutil.ReadFile(getFixturePath("clients.json"))
	assert.Nil(t, err)

	var clients []oauth.BaseClient

	err = json.Unmarshal(bytes, &clients)
	assert.Nil(t, err)
}
