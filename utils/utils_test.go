package utils_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth/utils"
)

func TestB64Encode(t *testing.T) {
	assert.Equal(
		t,
		[]byte(`SSBhbSB5b3VyIGZhdGhlcg`),
		utils.B64Encode([]byte(`I am your father`)),
	)
}

func TestBasicAuth(t *testing.T) {
	generator := func(auth string, username string, password string) func(t *testing.T) {
		return func(t *testing.T) {
			u, p, _ := utils.ParseBasicAuth(auth)
			assert.Equal(t, username, u)
			assert.Equal(t, password, p)
		}
	}

	t.Run("Correct", generator("Basic aGVsbG86bWU=", "hello", "me"))
	t.Run("No Prefix", generator("aGVsbG86bWU=", "", ""))
	t.Run("Not b64", generator("aGVsbG86bW?", "", ""))
	t.Run("Empty", generator("", "", ""))
}

func TestRandomString(t *testing.T) {
	generator := func(l int) func(t *testing.T) {
		return func(t *testing.T) {
			str, _ := utils.RandomString(l)
			assert.Equal(t, l, len(str))
		}
	}

	for i := 30; i <= 34; i++ {
		t.Run(strconv.Itoa(i), generator(i))
	}
}
