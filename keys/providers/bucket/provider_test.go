package bucket

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/providers/utils"
	"gitlab.com/thegalabs/go/oauth/mocks"
)

func now() keys.Now {
	return 100
}

func TestProvide(t *testing.T) {
	m := &mocks.BucketObject{}
	cipher, _ := utils.MakeCipher("1X/GwXETZSMkr/nqByLdxMs+2Mo+1cDREwmT+6ZJt1A=")

	provider := &Provider{m, cipher}

	m.On("Read").Return(nil, int64(0), nil)
	m.On("Write", mock.Anything, int64(0)).Return(nil)

	r, err := provider.Provide(nil, now, 10, 30, 20)

	m.AssertExpectations(t)

	assert.Nil(t, err)
	assert.NotNil(t, r)
}
