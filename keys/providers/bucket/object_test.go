//go:build bucket
// +build bucket

package bucket

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/providers/utils"
)

func TestNewObject(t *testing.T) {
	obj, err := newObject(
		context.Background(),
		os.Getenv("TEST_BUCKET"),
		"keys",
		10*time.Second)

	ring, gen, err := obj.Read()
	fmt.Printf("%t %d %s\n", ring != nil, gen, err)

	ring, _ = utils.RingToBytes(keys.NewRing())

	err = obj.Write(ring, 1605606030981225)
	fmt.Printf("%s\n", err)
}
