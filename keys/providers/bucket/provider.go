// Package bucket stores keys in a google bucket
package bucket

import (
	"context"
	"crypto/cipher"
	"time"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/providers/utils"
)

// Provider pulls keys from a bucket
type Provider struct {
	obj object
	gcm cipher.AEAD
}

// New makes a bucket provider
func New(
	ctx context.Context,
	bucketName string,
	objName string,
	timeout time.Duration,
	key string) (keys.KeyRingProvider, error) {
	obj, err := newObject(ctx, bucketName, objName, timeout)
	if err != nil {
		return nil, err
	}

	gcm, err := utils.MakeCipher(key)
	if err != nil {
		return nil, err
	}

	return &Provider{obj, gcm}, nil
}

func (p *Provider) transaction(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (newRing *keys.KeyRing, err error) {
	t := now()
	var gen int64
	var b []byte

	if b, gen, err = p.obj.Read(); err != nil {
		return
	}

	// There was no key
	if b == nil {
		if r != nil {
			newRing = r
		} else {
			newRing = keys.NewRing()
		}
	} else {
		if newRing, err = utils.RingFromSecret(b, p.gcm); err != nil {
			return
		}
		if !newRing.IsDirty(t, benchTime) {
			// It works -> great, we can use it, no need to rewrite it
			return
		}
	}

	// Cleanup whatever ring we have
	if err = newRing.Cleanup(t, benchTime, lifeTime, keepFor); err != nil {
		return
	}

	if b, err = utils.RingToSecret(newRing, p.gcm); err != nil {
		return
	}

	err = p.obj.Write(b, gen)

	return
}

// Provide retrieves or create a key ring
func (p *Provider) Provide(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (ring *keys.KeyRing, err error) {
	for i := 0; i < 5; i++ {
		ring, err = p.transaction(r, now, benchTime, lifeTime, keepFor)
		if err == ErrKeyWasChanged {
			continue
		}
		return
	}

	return nil, nil
}
