// Package local stores keys locally
package local

import (
	"gitlab.com/thegalabs/go/oauth/keys"
)

// Provider stores the key in memory
type Provider struct{}

// New makes a bucket provider
func New() keys.KeyRingProvider {
	return &Provider{}
}

// Provide retrieves or create a key ring
func (p *Provider) Provide(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (*keys.KeyRing, error) {
	t := now()
	if r == nil {
		r = keys.NewRing()
	}
	if !r.IsDirty(t, benchTime) {
		return r, nil
	}

	if err := r.Cleanup(t, benchTime, lifeTime, keepFor); err != nil {
		return nil, err
	}
	return r, nil
}
