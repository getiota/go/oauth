package utils

import (
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth/keys"
)

func TestBytes(t *testing.T) {
	r := keys.NewRing()
	err := r.Cleanup(50, 10, 20, 30)
	assert.Nil(t, err)

	b, err := RingToBytes(r)
	assert.Nil(t, err)
	assert.NotNil(t, b)

	ring, err := RingFromBytes(b)
	assert.Nil(t, err)
	assert.True(t, r.Equal(ring, false))
}

func TestSecret(t *testing.T) {
	gcm, _ := MakeCipher(base64.StdEncoding.EncodeToString(randomBytes(t, 32)))

	r := keys.NewRing()
	err := r.Cleanup(50, 10, 20, 30)
	assert.Nil(t, err)

	b, err := RingToSecret(r, gcm)
	assert.Nil(t, err)
	assert.NotNil(t, b)

	ring, err := RingFromSecret(b, gcm)
	assert.Nil(t, err)
	assert.True(t, r.Equal(ring, false))
}
