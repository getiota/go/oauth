package utils

import (
	"crypto/rand"
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"
)

func randomBytes(t *testing.T, len int) []byte {
	bytes := make([]byte, len)
	_, err := rand.Read(bytes)
	assert.Nil(t, err)
	return bytes
}

func TestMakeCipher32(t *testing.T) {
	cipher, err := MakeCipher(base64.StdEncoding.EncodeToString(randomBytes(t, 32)))

	assert.Nil(t, err)
	assert.NotNil(t, cipher)
}

func TestMakeCipherFail(t *testing.T) {
	cipher, err := MakeCipher(base64.StdEncoding.EncodeToString(randomBytes(t, 31)))

	assert.NotNil(t, err)
	assert.Nil(t, cipher)
}

func TestEncryptDecrypt(t *testing.T) {
	cipher, _ := MakeCipher(base64.StdEncoding.EncodeToString(randomBytes(t, 32)))

	raw := randomBytes(t, 100)

	enc, err := encrypt(raw, cipher)
	assert.Nil(t, err)
	assert.NotNil(t, enc)

	dec, err := decrypt(enc, cipher)
	assert.Nil(t, err)
	assert.Equal(t, raw, dec)
}
