package utils

import (
	"bytes"
	"crypto/cipher"
	"encoding/gob"

	"gitlab.com/thegalabs/go/oauth/keys"
)

// RingFromBytes uses gob encoding
func RingFromBytes(b []byte) (*keys.KeyRing, error) {
	var r keys.KeyRing
	dec := gob.NewDecoder(bytes.NewBuffer(b))

	if err := dec.Decode(&r); err != nil {
		return nil, err
	}
	return &r, nil
}

// RingToBytes uses gob encoding
func RingToBytes(r *keys.KeyRing) ([]byte, error) {
	var network bytes.Buffer

	enc := gob.NewEncoder(&network)
	if err := enc.Encode(r); err != nil {
		return nil, err
	}
	return network.Bytes(), nil
}

// RingFromSecret decrypts a ring
func RingFromSecret(b []byte, gcm cipher.AEAD) (*keys.KeyRing, error) {
	dec, err := decrypt(b, gcm)
	if err != nil {
		return nil, err
	}
	return RingFromBytes(dec)
}

// RingToSecret encrypts a ring
func RingToSecret(r *keys.KeyRing, gcm cipher.AEAD) ([]byte, error) {
	b, err := RingToBytes(r)
	if err != nil {
		return nil, err
	}
	return encrypt(b, gcm)
}
