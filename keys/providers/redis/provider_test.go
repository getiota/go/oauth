//go:build !no_redis
// +build !no_redis

package redis

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth/keys"
)

var ctx = context.Background()

func getTestHost() string {
	host := os.Getenv("REDIS_TEST_HOST")
	if host == "" {
		host = "localhost"
	}
	return fmt.Sprintf("%s:6379", host)
}

func TestProvider(t *testing.T) {
	p := New(ctx, getTestHost(), "", 0).(*Provider)

	now := func() keys.Now {
		return 1000
	}

	// Deleting current value
	p.rdb.Del(p.context, key)

	r, err := p.Provide(nil, now, 10, 100, 11)
	assert.Nil(t, err)

	r2, err := p.Provide(nil, now, 10, 100, 11)
	assert.Nil(t, err)

	assert.True(t, r.Equal(r2, false))
}

func TestConcurrency(t *testing.T) {
	rand.Seed(time.Now().Unix())

	p := New(ctx, getTestHost(), "", 0)

	const bench = keys.BenchTime(250 * time.Millisecond)
	const life = keys.LifeTime(500 * time.Millisecond)
	const keepFor = keys.KeepFor(100 * time.Millisecond)

	now := func() keys.Now {
		return keys.Now(time.Now().UnixNano())
	}

	// truth is the actual share ring
	truth, err := p.Provide(nil, now, 10, 100, 11)
	assert.Nil(t, err)

	const concurrencies = 10
	const repeat = 10
	// There is one ring for each app that will try to access the ring
	rings := make([]*keys.KeyRing, concurrencies)

	c := make(chan error)

	for j := 0; j < repeat; j++ {
		for i := range rings {
			go func(i int) {
				// Sleep for a bit
				time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond) //nolint: gosec

				// If the ring is clean then it should be the same one
				// At the exception of expired keys because invalid expired keys are not considered dirty
				if rings[i] != nil && !rings[i].IsDirty(now(), bench) {
					assert.True(t, truth.Equal(rings[i], true))
				}

				// Ring should be updated
				r, err := p.Provide(rings[i], now, bench, life, keepFor)
				if err == nil {
					rings[i] = r
					// there might be some issue with time here :/
					if truth.IsDirty(now(), bench) {
						// If truth is outdated then we have the right one
						truth = r
					} else {
						// If not, we should have truth
						assert.True(t, truth.Equal(rings[i], true))
					}
				}

				c <- err
				rings[i] = r
			}(i)
		}

		for range rings {
			err := <-c
			assert.Nil(t, err)
		}
	}
}
