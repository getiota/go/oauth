// Package redis stores keys on a redis
package redis

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/providers/utils"
)

const key = "ring"

// Provider writes and reads keys from a redis
type Provider struct {
	rdb     *redis.Client
	context context.Context
}

// New creates a new provider
func New(ctx context.Context, addr string, password string, db int) keys.KeyRingProvider {
	rdb := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})

	return &Provider{rdb, ctx}
}

// Provide tries to retrieve a key from redis and rewrite it
// nolint: funlen
func (p *Provider) Provide(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (*keys.KeyRing, error) {
	var newRing *keys.KeyRing
	var err error

	txf := func(tx *redis.Tx) error {
		// now needs to be refreshed everytime we try otherwise we might receive keys that are only
		// available in the future
		t := now()
		// First we try to retrieve a key
		if newRing, err = retrieve(p.context, tx); err != nil {
			return err
		}

		// There was no key
		if newRing == nil {
			if r != nil {
				newRing = r
			} else {
				newRing = keys.NewRing()
			}
		} else if !newRing.IsDirty(t, benchTime) {
			// It works -> great, we can use it, no need to rewrite it
			return nil
		}

		// Actual operation (local in optimistic lock).
		if err = newRing.Cleanup(t, benchTime, lifeTime, keepFor); err != nil {
			return err
		}

		raw, err := utils.RingToBytes(newRing)
		if err != nil {
			return err
		}

		// Operation is committed only if the watched key remain unchanged
		_, err = tx.TxPipelined(p.context, func(pipe redis.Pipeliner) error {
			pipe.Set(p.context, key, raw, 0)
			return nil
		})
		return err
	}

	// 5 max tries
	for i := 0; i < 5; i++ {
		err := p.rdb.Watch(p.context, txf, key)
		if err == nil {
			return newRing, nil
		}
		// Optimistic lock lost. Retry.
		if err == redis.TxFailedErr {
			continue
		}
		// Return any other error.
		return nil, err
	}

	return nil, fmt.Errorf("exceeded max tries to ensure keyring\nLast err: %s", err)
}

func retrieve(ctx context.Context, tx *redis.Tx) (*keys.KeyRing, error) {
	get := tx.Get(ctx, key)
	raw, err := get.Bytes()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}
	return utils.RingFromBytes(raw)
}
