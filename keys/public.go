package keys

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/binary"
)

// PublicKey holds data about a public key
type PublicKey struct {
	ID       string
	ExpireAt int64 `json:"-"`
	Key      *ecdsa.PublicKey
}

// ShouldExpire returns true if the key should be removed
func (k *PublicKey) ShouldExpire(now int64) bool {
	return k.ExpireAt < now
}

// MarshalBinary implements the gobbing protocol
func (k PublicKey) MarshalBinary() ([]byte, error) {
	data, err := x509.MarshalPKIXPublicKey(k.Key)
	if err != nil {
		return nil, err
	}

	var b bytes.Buffer
	if _, err = b.Write(data); err != nil {
		return nil, err
	}

	if err = binary.Write(&b, binary.LittleEndian, k.ExpireAt); err != nil {
		return nil, err
	}

	if _, err = b.WriteString(k.ID); err != nil {
		return nil, err
	}
	b.WriteByte(0)
	return b.Bytes(), nil
}

// UnmarshalBinary implements the gobbing protocol
func (k *PublicKey) UnmarshalBinary(data []byte) (err error) {
	var b = bytes.NewBuffer(data)

	keyData := make([]byte, 91)
	if _, err = b.Read(keyData); err != nil {
		return
	}

	var pub interface{}
	if pub, err = x509.ParsePKIXPublicKey(keyData); err != nil {
		return
	}
	k.Key = pub.(*ecdsa.PublicKey)

	if err = binary.Read(b, binary.LittleEndian, &k.ExpireAt); err != nil {
		return
	}

	var id []byte
	if id, err = b.ReadBytes(0); err != nil {
		return
	}
	k.ID = string(id[:len(id)-1])
	return
}
