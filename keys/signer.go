// Package keys provides a signer implementation that relies on a key ring
// that can be rotated in a thread / instance safe manner
package keys

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
)

// KeyRingProvider is an object that manages a key repository
type KeyRingProvider interface {
	// Provide returns a clean key ring or an error. It is called every time the current keyring is dirty.
	// This method is called within a mutex lock so it should do its best to return fast
	Provide(*KeyRing, func() Now, BenchTime, LifeTime, KeepFor) (*KeyRing, error)
}

// DefaultSigner manages rotating signature keys
//
// Key rotation is evaluated every time the JWKS() function is called.
// Signing will also not occurred if the manager is not in its nominal state.
//
// Key rotation is done so that:
// - key rotations operations should be triggered every time keys are retrieved or a token is signed
// - keys are rotated approximately every `KeyRotation`. Approximately because if a key has not been used for a while,
// it will be used at least once before the rotation process is triggered. This should not happen since the `JWKS()`
// function should be called regularly by the gateway
// - before a key is active, it is benched and returned by `JWKS` for at least MinKeyBench. This is so that
// the gateway has time to retrieve the key before using it.
type DefaultSigner struct {
	keyRing *KeyRing

	mu sync.Mutex

	// MinKeyBench is the min amount of time a key is benched before become active
	MinKeyBench BenchTime
	// KeyLifetime is the delay after which a key is rotated
	KeyLifetime LifeTime
	// KeepKeyFor is the time a key is kept after it expired
	KeepKeyFor KeepFor

	provider KeyRingProvider
}

// NewSigner is the default constructor for DefaultSigner
func NewSigner(minKeyBench, keyLifetime, keepKeyFor int64, provider KeyRingProvider) (*DefaultSigner, error) {
	m := DefaultSigner{
		MinKeyBench: BenchTime(minKeyBench),
		KeyLifetime: LifeTime(keyLifetime),
		KeepKeyFor:  KeepFor(keepKeyFor),
		provider:    provider,
	}

	var err error
	now := func() Now {
		return Now(time.Now().Unix())
	}
	if e := log.Debug(); e.Enabled() {
		start := time.Now()
		defer func() {
			e.Msgf("Providing took %s", time.Since(start).String())
		}()
	}

	if m.keyRing, err = provider.Provide(nil, now, m.MinKeyBench, m.KeyLifetime, m.KeepKeyFor); err != nil {
		return nil, err
	}

	return &m, nil
}

// TokenLifetime returns how long the key is kept after it expired
func (m *DefaultSigner) TokenLifetime() int64 {
	return int64(m.KeepKeyFor)
}

// ensure makes sure that everything is ok with the key ring
// the difficulty with this function is to:
// - ensure thread safety
// - allow concurrent read/write operations with the provider (redis...)
// the base requirement is that everybody must have the same keyring
func (m *DefaultSigner) ensure() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	now := func() Now {
		return Now(time.Now().Unix())
	}

	if m.keyRing != nil && !m.keyRing.IsDirty(now(), m.MinKeyBench) {
		// Nothing to do
		return nil
	}

	if e := log.Debug(); e.Enabled() {
		start := time.Now()
		defer func() {
			e.Msgf("Providing took %s", time.Since(start).String())
		}()
	}

	r, err := m.provider.Provide(m.keyRing, now, m.MinKeyBench, m.KeyLifetime, m.KeepKeyFor)
	if err != nil {
		return err
	}
	m.keyRing = r
	return nil
}

// PublicKeys returns the available public keys, it also rotates the keys if required
// Also returns the remaining time for which the keys will be valid
func (m *DefaultSigner) PublicKeys() ([]PublicKey, int64, error) {
	if err := m.ensure(); err != nil {
		return nil, 0, err
	}
	return m.keyRing.PublicKeys()
}

// KeyID returns the active key id after ensuring everything is ok
func (m *DefaultSigner) KeyID() (string, error) {
	if err := m.ensure(); err != nil {
		return "", err
	}
	return m.keyRing.active.id, nil
}

// Sign returns a signature
// The provided key ID must match the one returned by KeyID()
func (m *DefaultSigner) Sign(keyID string, data []byte) ([]byte, error) {
	if m.keyRing.active.id != keyID {
		return nil, errors.New("provided Key ID did not match active key")
	}
	return sign(m.keyRing.active.key, data)
}

func sign(key *ecdsa.PrivateKey, data []byte) ([]byte, error) {
	var err error
	hash := sha256.Sum256(data)

	// Adapted from https://github.com/dgrijalva/jwt-go/blob/master/ecdsa.go
	if r, s, err := ecdsa.Sign(rand.Reader, key, hash[:]); err == nil {
		keyBytes := 256 / 8

		// We serialize the outpus (r and s) into big-endian byte arrays and pad
		// them with zeros on the left to make sure the sizes work out. Both arrays
		// must be keyBytes long, and the output must be 2*keyBytes long.
		rBytes := r.Bytes()
		rBytesPadded := make([]byte, keyBytes)
		copy(rBytesPadded[keyBytes-len(rBytes):], rBytes)

		sBytes := s.Bytes()
		sBytesPadded := make([]byte, keyBytes)
		copy(sBytesPadded[keyBytes-len(sBytes):], sBytes)

		out := rBytesPadded
		out = append(out, sBytesPadded...)
		return out, nil
	}
	return nil, err
}
