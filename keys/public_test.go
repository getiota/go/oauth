package keys

import (
	"bytes"
	"encoding/gob"
	"testing"

	"github.com/stretchr/testify/assert"
)

func comparePublicKey(t *testing.T, expected *PublicKey, actual *PublicKey) {
	if expected == nil {
		assert.Nil(t, actual)
		return
	}
	assert.Equal(t, expected.ID, actual.ID)
	assert.Equal(t, expected.ExpireAt, actual.ExpireAt)
	assert.True(t, expected.Key.Equal(actual.Key))
}

func TestPublicKeyGobbing(t *testing.T) {
	k := newPublicKey(t)

	var network bytes.Buffer

	enc := gob.NewEncoder(&network)
	err := enc.Encode(k)
	assert.Nil(t, err)

	dec := gob.NewDecoder(&network)
	var v PublicKey
	err = dec.Decode(&v)
	assert.Nil(t, err)

	comparePublicKey(t, &k, &v)
}
