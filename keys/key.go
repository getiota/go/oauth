package keys

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/binary"
)

type key struct {
	id        string
	key       *ecdsa.PrivateKey
	notBefore int64
	notAfter  int64
}

func (k *key) isUsable(t int64) bool {
	return k.notBefore <= t && k.notAfter > t
}

func (k key) MarshalBinary() ([]byte, error) {
	data, err := x509.MarshalECPrivateKey(k.key)
	if err != nil {
		return nil, err
	}

	var b bytes.Buffer
	if _, err = b.Write(data); err != nil {
		return nil, err
	}

	if err = binary.Write(&b, binary.LittleEndian, k.notBefore); err != nil {
		return nil, err
	}
	if err = binary.Write(&b, binary.LittleEndian, k.notAfter); err != nil {
		return nil, err
	}

	if _, err = b.WriteString(k.id); err != nil {
		return nil, err
	}
	b.WriteByte(0)
	return b.Bytes(), nil
}

func (k *key) UnmarshalBinary(data []byte) (err error) {
	var b = bytes.NewBuffer(data)

	keyData := make([]byte, 121)
	if _, err = b.Read(keyData); err != nil {
		return
	}

	if k.key, err = x509.ParseECPrivateKey(keyData); err != nil {
		return
	}

	if err = binary.Read(b, binary.LittleEndian, &k.notBefore); err != nil {
		return
	}

	if err = binary.Read(b, binary.LittleEndian, &k.notAfter); err != nil {
		return
	}

	var id []byte
	if id, err = b.ReadBytes(0); err != nil {
		return
	}
	k.id = string(id[:len(id)-1])
	return
}

func (k *key) public(expiry int64) *PublicKey {
	return &PublicKey{
		ID:       k.id,
		ExpireAt: expiry,
		Key:      &k.key.PublicKey,
	}
}
