package keys

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/gob"
	"testing"

	"github.com/stretchr/testify/assert"
)

func compareKey(t *testing.T, expected *key, actual *key) {
	if expected == nil {
		assert.Nil(t, actual)
		return
	}
	assert.Equal(t, expected.id, actual.id)
	assert.Equal(t, expected.notBefore, actual.notBefore)
	assert.Equal(t, expected.notAfter, actual.notAfter)
	assert.True(t, expected.key.Equal(actual.key))
}

func TestKeyGobbing(t *testing.T) {
	k := &key{
		id:        "hello0",
		notBefore: 100,
		notAfter:  20,
	}

	var err error
	k.key, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	assert.Nil(t, err)

	var network bytes.Buffer

	enc := gob.NewEncoder(&network)
	err = enc.Encode(k)
	assert.Nil(t, err)

	dec := gob.NewDecoder(&network)
	var v key
	err = dec.Decode(&v)
	assert.Nil(t, err)

	compareKey(t, k, &v)
}
