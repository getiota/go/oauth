// +build !go1.15

package keys

// Equal returns true if the 2 are identical
func (k *PublicKey) Equal(other *PublicKey) bool {
	if other == nil {
		return false
	}
	return k.ID == other.ID &&
		k.ExpireAt == other.ExpireAt
}

// Equal returns true if the 2 keyrings are identical
func (r *KeyRing) Equal(other *KeyRing, ignoreExp bool) bool {
	compareKeys := func(k1 *key, k2 *key) bool {
		if k1 == nil {
			return k2 == nil
		}
		if k2 == nil {
			return false
		}
		return k1.id == k2.id &&
			k1.notBefore == k2.notBefore &&
			k1.notAfter == k2.notAfter
	}
	if other.active == nil {
		return false
	}

	if !compareKeys(r.active, other.active) {
		return false
	}
	if !compareKeys(r.benched, other.benched) {
		return false
	}

	if ignoreExp {
		return true
	}

	if len(r.expired) != len(other.expired) {
		return false
	}
	if len(r.expired) == 0 {
		return true
	}

	for i := range r.expired {
		if !r.expired[i].Equal(&other.expired[i]) {
			return false
		}
	}
	return true
}
