package keys

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/gob"
	"errors"

	"gitlab.com/thegalabs/go/oauth/utils"
)

// Now is an int64 alias that represents the curren time
type Now int64

// BenchTime is an int64 alias that represents how a key should exist before it is used
type BenchTime int64

// LifeTime is an int64 alias that represents the duration for which a key can be use
type LifeTime int64

// KeepFor is an int64 alias that represents how long the key should be kept after it expired
type KeepFor int64

// KeyRing manages a set of keys
type KeyRing struct {
	active  *key
	benched *key
	expired []PublicKey
}

// NewRing creates a new keyring
func NewRing() *KeyRing {
	return &KeyRing{expired: []PublicKey{}}
}

// Panics if active is nil
func (r *KeyRing) shouldBench(t Now, benchtime BenchTime) bool {
	// r.log(t, "shb", "value: %t, bench: %d", int64(t)+int64(benchtime) > r.active.notAfter, benchtime/BenchTime(time.Millisecond))
	// if the active key expires less than benchtime from now
	return r.benched == nil && int64(t)+int64(benchtime) > r.active.notAfter
}

func (r *KeyRing) bench(t Now, benchtime BenchTime, lifetime LifeTime) (err error) {
	notBefore := int64(t) + int64(benchtime)
	if r.benched, err = r.newKey(notBefore, lifetime); err != nil {
		// r.log(t, "ben", "Failed: %s", err)
		return
	}
	// r.log(t, "ben", "")
	r.active.notAfter = notBefore
	return
}

// This should really almost never occur
func (r *KeyRing) makeActive(t Now, lifetime LifeTime) (err error) {
	if r.active, err = r.newKey(int64(t), lifetime); err != nil {
		return
	}
	r.active.notBefore = int64(t)
	return
}

// Panics if benched is nil
func (r *KeyRing) shouldRotate(t Now) bool {
	// r.log(t, "shr", "usable: %t", r.active.isUsable(int64(t)))
	return !r.active.isUsable(int64(t))
}

func (r *KeyRing) rotate(t Now, lifetime LifeTime, keepfor KeepFor) error {
	pub := r.active.public(int64(t) + int64(keepfor))
	// r.log(t, "rot", "bench: %t", r.benched != nil)

	r.expired = append(r.expired, *pub)
	r.active = r.benched
	// this is to make sure that a key that was just made active can still be used for its lifetime
	r.active.notAfter = int64(t) + int64(lifetime)
	r.benched = nil
	return nil
}

// purge expired keys that should not exist any more
func (r *KeyRing) purge(t Now) {
	filtered := []PublicKey{}
	for _, k := range r.expired {
		if !k.ShouldExpire(int64(t)) {
			filtered = append(filtered, k)
		}
	}
	r.expired = filtered
}

func (r *KeyRing) newKey(notBefore int64, lifetime LifeTime) (*key, error) {
	// Probability of having 2 keys that exist at the same time with the same name?
	id, err := utils.RandomString(6)
	if err != nil {
		return nil, err
	}
	k, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}
	return &key{id, k, notBefore, notBefore + int64(lifetime)}, nil
}

// IsDirty returns true if the key ring needs an operation
func (r *KeyRing) IsDirty(t Now, benchtime BenchTime) bool {
	return r.active == nil || r.shouldBench(t, benchtime) || r.shouldRotate(t)
}

// Cleanup makes the keyring not dirty
func (r *KeyRing) Cleanup(t Now, benchtime BenchTime, lifetime LifeTime, keepfor KeepFor) (err error) {
	// r.log(t, "cla", "")
	if r.active == nil {
		if err = r.makeActive(t, lifetime); err != nil {
			return
		}
		// r.log(t, "cla", "created")
	}

	if r.shouldBench(t, benchtime) {
		if err = r.bench(t, benchtime, lifetime); err != nil {
			return
		}
	}

	if r.shouldRotate(t) {
		if err = r.rotate(t, lifetime, keepfor); err != nil {
			return
		}
	}

	r.purge(t)
	return
}

// PublicKeys returns the public keys associated with the key ring
func (r *KeyRing) PublicKeys() (keys []PublicKey, expireAt int64, err error) {
	keys = r.expired

	adder := func(k *key) {
		if k != nil {
			pub := r.active.public(0) // TODO: pick a better expiration date?
			keys = append(keys, *pub)
		}
	}
	adder(r.active)
	adder(r.benched)

	if r.active != nil {
		expireAt = r.active.notAfter
	}

	return
}

// Key returns the active key
func (r *KeyRing) Key() (string, *ecdsa.PrivateKey) {
	return r.active.id, r.active.key
}

// MarshalBinary implmements the gob protocol
func (r KeyRing) MarshalBinary() (_ []byte, err error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)

	writer := func(k *key) error {
		if k != nil {
			_ = b.WriteByte(1)
			err = enc.Encode(k)
			return err
		}
		return b.WriteByte(0)
	}
	if err = writer(r.benched); err != nil {
		return
	}
	if err = writer(r.active); err != nil {
		return
	}

	if err = enc.Encode(r.expired); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// UnmarshalBinary implmements the gob protocol
func (r *KeyRing) UnmarshalBinary(data []byte) (err error) {
	var b = bytes.NewBuffer(data)
	dec := gob.NewDecoder(b)

	reader := func() (*key, error) {
		isnil, err := b.ReadByte()
		if err != nil {
			return nil, err
		}
		if isnil == 0 {
			return nil, nil
		}
		if isnil != 1 {
			return nil, errors.New("unexpected byte")
		}
		var k key
		err = dec.Decode(&k)
		return &k, err
	}

	if r.benched, err = reader(); err != nil {
		return
	}
	if r.active, err = reader(); err != nil {
		return
	}

	return dec.Decode(&r.expired)
}

// log is useful to debug parallelism...
// func (r *KeyRing) log(t Now, name, format string, args ...interface{}) {
// 	str := fmt.Sprintf(format, args...)

// 	act := r.active
// 	if act == nil {
// 		act = &key{}
// 	}

// 	fmt.Printf(
// 		"(%p %d %s) (a: %s | %d | %d, b: %t) %s\n",
// 		r,
// 		t/Now(time.Millisecond),
// 		name,
// 		act.id,
// 		act.notBefore/int64(time.Millisecond),
// 		act.notAfter/int64(time.Millisecond),
// 		r.benched != nil,
// 		str,
// 	)
// }
