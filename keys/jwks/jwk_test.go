package jwks_test

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/json"
	mrand "math/rand"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/jwks"
)

func TestJWKJSONMarshalling(t *testing.T) {
	key := jwks.ECJWK{"1", "2", "3", "4", "5", "6"}

	bs, err := json.Marshal(key)

	assert.Nil(t, err)
	assert.Equal(t, `{"kty":"1","kid":"2","alg":"3","crv":"4","x":"5","y":"6"}`, string(bs))
}

func newPublicKey(t *testing.T) keys.PublicKey {
	private, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	assert.Nil(t, err)
	return keys.PublicKey{
		ID:       uuid.Must(uuid.NewV4()).String(),
		ExpireAt: mrand.Int63(), //nolint: gosec
		Key:      &private.PublicKey,
	}
}

func TestFromKey(t *testing.T) {
	jwk, err := jwks.FromKey(newPublicKey(t))

	assert.Nil(t, err)
	assert.NotEmpty(t, jwk.X)
	assert.NotEmpty(t, jwk.Y)
}
