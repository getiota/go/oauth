package jwks

import (
	"crypto/elliptic"
	"encoding/base64"
	"errors"
	"fmt"

	"gitlab.com/thegalabs/go/oauth/keys"
)

// ECJWK represents a JWK for an elliptic curve key
type ECJWK struct {
	Kty string `json:"kty,omitempty"`
	Kid string `json:"kid,omitempty"`
	Alg string `json:"alg,omitempty"`
	Crv string `json:"crv,omitempty"`
	X   string `json:"x,omitempty"`
	Y   string `json:"y,omitempty"`
}

// Equal returns true if the 2 are identical
func (k *ECJWK) Equal(other *ECJWK) bool {
	if other == nil {
		return false
	}
	return k.Kty == other.Kty &&
		k.Kid == other.Kid &&
		k.Alg == other.Alg &&
		k.Crv == other.Crv &&
		k.X == other.X &&
		k.Y == other.Y
}

func curveName(crv elliptic.Curve) (string, error) {
	switch crv {
	case elliptic.P256():
		return "P-256", nil
	case elliptic.P384():
		return "P-384", nil
	case elliptic.P521():
		return "P-521", nil
	default:
		return "", fmt.Errorf("unsupported/unknown elliptic curve")
	}
}

// FromKey converts a public key to a json web key
func FromKey(k keys.PublicKey) (*ECJWK, error) {
	// from https://github.com/square/go-jose/blob/0a67ce9b0693b316cba5e8ab8a1f0b7a9d609ac7/jwk.go#L532
	if k.Key.X == nil || k.Key.Y == nil {
		return nil, errors.New("invalid EC key (nil, or X/Y missing)")
	}

	name, err := curveName(k.Key.Curve)
	if err != nil {
		return nil, err
	}

	b64encoding := base64.RawURLEncoding

	return &ECJWK{
		Alg: "ES256", // TODO: remove hardcoded
		Kty: "EC",
		Kid: k.ID,
		Crv: name,
		X:   b64encoding.EncodeToString(k.Key.X.Bytes()),
		Y:   b64encoding.EncodeToString(k.Key.Y.Bytes()),
	}, nil
}
