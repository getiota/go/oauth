package oauth

// AuthenticateClient returns claims for a client grant
// Default implementation returns a claim with
func AuthenticateClient(c *Client) *Claims {
	claims := (*c).BaseClaims()
	claims.Scope = (*c).GetScope(ClientCredentials)
	return claims
}
