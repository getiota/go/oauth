#!/usr/bin/env bash

mockery --name=object --structname BucketObject --filename bucket_object.go --dir providers/bucket --case underscore
